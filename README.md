# inspircd-modules

Enter your InspIRCd source directory.

Copy the .cpp file to your InspIRCd source director's **src/modules/**. 

## m_nick_conn_join.cpp
Add the following to your **src/modules/Makefile**;

```
m_nick_conn_join.so: m_nick_conn_join.cpp ../../include/modules.h ../../include/users.h ../../include/channels.h ../../include/base.h ../../include/inspircd_config.h ../../include/inspircd.h ../../include/configreader.h
     @../../make/run-cc.pl $(CC) -pipe -I../../include $(FLAGS)  $(PICLDFLAGS)  -shared -export-dynamic -o m_nick_conn_join.so m_nick_conn_join.cpp

```

Then run this;
```
./configure -modupdate
make
rc=$?
if [[ $rc != 0 ]]; then
    exit $rc
fi

make install
```

### Configuration tags

Enable the module by adding this to your **config/modules.conf**;
```
<module name="m_nick_conn_join.so">
<nickconnjoin mask="kjelle!*@*" channel="#opers,#users">
<nickconnjoin mask="CLAN-*!*@*" channel="#clan,#users">
```
If any connecting user matches one of the masks the user will be joined to one or more channels, separated by comma.


