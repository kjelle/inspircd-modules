/*       +------------------------------------+
 *       | Inspire Internet Relay Chat Daemon |
 *       +------------------------------------+
 *
 * This is an InspIRCd module made by Kjell Tore Fossbakk
 * at 27.11.2013. 
 *
 * This program is free but copyrighted software; see
 *            the file COPYING for details.
 *
 * ---------------------------------------------------
 */

#include "inspircd.h"

/* $ModDesc: Forces users matching a mask to join the specified channel(s) on connect */

typedef std::map<std::string,std::vector<std::string> > nickconnjoins_t ;

class ModuleNickConnJoin : public Module
{
	private:
        nickconnjoins_t nickconnjoins ;
		std::string JoinChan;

		int tokenize(const std::string &str, std::vector<std::string> &tokens)
		{
			// skip delimiters at beginning.
			std::string::size_type lastPos = str.find_first_not_of(",", 0);
			// find first "non-delimiter".
			std::string::size_type pos = str.find_first_of(",", lastPos);

			while (std::string::npos != pos || std::string::npos != lastPos)
			{
				// found a token, add it to the vector.
				tokens.push_back(str.substr(lastPos, pos - lastPos));
				// skip delimiters. Note the "not_of"
				lastPos = str.find_first_not_of(",", pos);
				// find next "non-delimiter"
				pos = str.find_first_of(",", lastPos);
			}
			return tokens.size();
		}

	public:
		ModuleNickConnJoin(InspIRCd* Me)
			: Module(Me)
		{
			OnRehash(NULL, "");
			Implementation eventlist[] = { I_OnPostConnect, I_OnRehash };
			ServerInstance->Modules->Attach(eventlist, this, 2);
		}

		void Prioritize()
		{
			ServerInstance->Modules->SetPriority(this, I_OnPostConnect, PRIO_LAST);
		}


		virtual void OnRehash(User* user, const std::string &parameter)
		{
            ConfigReader Conf(ServerInstance);

            ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"Rehashing NickConnJoin!");

/*            for (nickconnjoins_t::iterator i = nickconnjoins.begin(); i != nickconnjoins.end(); i++)
            {
                delete i->second; 
            }*/
            nickconnjoins.clear();

            for(int index = 0; index < Conf.Enumerate("nickconnjoin"); index++)
            {
                std::string mask = Conf.ReadValue("nickconnjoin", "mask", index);
                std::string channels = Conf.ReadValue("nickconnjoin", "channel", index);
                std::vector<std::string> Joinchans;

                ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"Adding Mask: %s", mask.c_str());


                tokenize(channels,Joinchans) ;

                nickconnjoins[mask] = Joinchans;
            }
		}

		virtual ~ModuleNickConnJoin()
		{
/*            for (nickconnjoins_t::iterator i = nickconnjoins.begin(); i != nickconnjoins.end(); i++)
            {
                delete i->second; 
            }*/
            nickconnjoins.clear();
		}

		virtual Version GetVersion()
		{
			return Version("$Id: m_nick_conn_join.cpp 1 2013-11-26 23:45:11Z kjelle $", VF_VENDOR,API_VERSION);
		}

		virtual void OnPostConnect(User* user)
		{
/*        
            ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"User '%s' connecting", (user->nick).c_str() );
            ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"MakeHost: %s  MakeHostIP: %s", (user->MakeHost()).c_str(), (user->MakeHostIP()).c_str()) ;
            ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"GetFullRealHost: %s", (user->GetFullRealHost()).c_str());
*/
            for (nickconnjoins_t::iterator i = nickconnjoins.begin(); i != nickconnjoins.end(); i++)
            {
/*            
                ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"------------");
                ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"Mask: %s", (i->first).c_str());
                ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"MatchCIDR on GetFullRealHost: %d Match on GetFullRealHost: %d", ( (InspIRCd::MatchCIDR(user->GetFullRealHost(), i->first))  ),   ((InspIRCd::Match(user->GetFullRealHost(), i->first))) );            
                ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"------------");
*/

                if (((InspIRCd::MatchCIDR(user->MakeHost(), i->first)) || (InspIRCd::MatchCIDR(user->MakeHostIP(), i->first)) || ((InspIRCd::Match(user->GetFullRealHost(), i->first))) || ((InspIRCd::MatchCIDR(user->GetFullRealHost(), i->first))) )) 
                {
                    std::vector<std::string> Joinchans = i->second ;

			        for(std::vector<std::string>::iterator it = Joinchans.begin(); it != Joinchans.end(); it++)
        				if (ServerInstance->IsChannel(it->c_str(), ServerInstance->Config->Limits.ChanMax))
                        {
                            ServerInstance->Logs->Log("m_nick_conn_join",DEFAULT,"Forcing %s to join %s because of mask %s (%s)", 
                                (user->nick).c_str(),
                                it->c_str(),
                                (i->first).c_str(),
                                (user->GetFullRealHost()).c_str()
                            );

        					Channel::JoinUser(ServerInstance, user, it->c_str(), false, "", false, ServerInstance->Time());
                        }
                }
            }
		}

};


MODULE_INIT(ModuleNickConnJoin)
